const data = function($http, API_URL) {
  'ngInject';
  let allCities = [];
  $http.get(`${API_URL}/fixtures/cities.json`).then(res => {
    allCities = res.data.map(crunchData);
  });

  const getAllZones = () => ($http.get(`${API_URL}/fixtures/zones.json`).then(res => res.data));
  const getAllCities = () => ($http.get(`${API_URL}/fixtures/cities.json`).then(res => res.data.map(crunchData)));

  const getTravellerCities = (cityIDs) => {
    return allCities.filter(city => cityIDs.indexOf(city.cityId.toString()) > -1);
  };

  const getNonSelectedCities = (allCities, selectedCityIds) => {
    const allCitiesClone = [...allCities];
    _.remove(allCitiesClone, (city) => _.includes(selectedCityIds, city.cityId.toString()));
    return allCitiesClone;
  };

  const getHighlightedCity = (cities) => {
    const foundCity = _.find(cities, {
      highlighted: true
    });
    return foundCity ? foundCity.cityId.toString() : '';
  };


  const crunchData = (city) => (Object.assign({}, city, {
    highlighted: false
  }));

  const convertCityToIds = (cities) => {
    return cities.map(city => city.cityId).join(',');
  };

  const getRecommendedCities = () => {
    return [];
  };

  const checkIfCityExists = (selectedCities, cityToSearch) => {
    if (!cityToSearch) {
      return true;
    }
    const selectedCitiesClone = [...selectedCities];
    const shouldNotHaveCityName = (city) => (city.cityName !== cityToSearch.cityName);
    return selectedCitiesClone.every(shouldNotHaveCityName); //return true if the cityToSearch exists inside selectedCities arrays
  };

  return {
    getTravellerCities,
    getAllZones,
    getRecommendedCities,
    allCities,
    getAllCities,
    convertCityToIds,
    getNonSelectedCities,
    checkIfCityExists,
    getHighlightedCity
  };
};

export default data;
