import config from './index.config';
import routerConfig from './index.route';
import runBlock from './index.run';
import TravellersController from './components/travellers/travellers.controller';
import TravellerController from './components/traveller/traveller.controller';
import DataService from './dataStore/data.service';
import UsersCache from './dataStore/usersCache.service';


angular.module('client', ['ngTouch', 'ui.router', 'angucomplete', 'ngSails'])
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .service('DataMaster', DataService)
  .service('UsersCache', UsersCache)
  .controller('TravellersController', TravellersController)
  .controller('TravellerController', TravellerController)
  .constant('API_URL', location.origin.indexOf('localhost') > -1 ? 'http://localhost:1337' : location.origin);
