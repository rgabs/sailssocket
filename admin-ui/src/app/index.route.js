function routerConfig($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('traveller', {
      url: '/traveller/:travellerID',
      templateUrl: 'app/components/traveller/traveller.html',
      controller: 'TravellerController',
      controllerAs: 'traveller',
      resolve: {
        resolvedData(DataMaster) {
          const zones = DataMaster.getAllZones();
          const wizardCities = DataMaster.getRecommendedCities();
          const allCities = DataMaster.getAllCities();
          return Promise.all([zones, wizardCities, allCities]).then(res => ({
            zones: res[0],
            wizardCities: res[1],
            allCities: res[2]
          }));
        }
      }
    })
    .state('travellers', {
      url: '/',
      templateUrl: 'app/components/travellers/travellers.html',
      controller: 'TravellersController',
      controllerAs: 'travellers'
    });

  $urlRouterProvider.otherwise('/');
}

export default routerConfig;
