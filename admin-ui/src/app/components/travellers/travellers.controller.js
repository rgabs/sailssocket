class TravellersController {
  constructor($sails) {
    'ngInject';
    let vm = this;
    vm.users = [];
    $sails.get('/traveller')
      .success(res => {
        vm.users = res;
      })
      .error(err => console.log('---err---', err));

    $sails.on('traveller', (res) => {
      switch (res.verb) {
        case 'created': {
          vm.users.push(res.data);
        }
      }
    });
  }
}

export default TravellersController;
