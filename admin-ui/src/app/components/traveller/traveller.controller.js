class TravellerController {
  constructor(resolvedData, $sails, UsersCache, $stateParams, DataMaster) {
    'ngInject';
    let vm = this;
    vm.$sails = $sails;
    vm.highlightedZones = [];
    vm.wizardCitytoAdd = {};
    vm.user = {
      id: '',
      name: ''
    };
    vm.citytoRemove = {
      cityChosenBy: '',
      city: {}
    };
    vm.DataMaster = DataMaster;
    vm.$stateParams = $stateParams;
    vm.cities = [];
    vm.zones = resolvedData.zones;
    vm.travellerCities = [];
    vm.wizardCities = [];
    vm.modalVisibility = false;
    $sails.get(`/traveller/${$stateParams.travellerID}`)
      .success(res => {
        vm.travellerCities = DataMaster.getTravellerCities(res.selectedCities);
        vm.highlightedZones = res.visibleZones;
        vm.wizardCities = DataMaster.getTravellerCities(res.wizardCities);
        vm.user = {
          id: res.id,
          name: res.name
        };
        vm.cities = DataMaster.getNonSelectedCities(resolvedData.allCities, [...res.selectedCities, ...res.wizardCities]);
        vm.highlightCityWithId(res.highlightedCity);
      })
      .error(err => console.error('---in api---', err));

    $sails.on('traveller', (res) => {
      console.log('log:', 'res', res);
      if (res.verb === 'updated' && 'highlightedCity' in res.data) {
        vm.highlightCityWithId(res.data.highlightedCity);
      } else if (res.verb === 'updated' && 'selectedCities' in res.data) {
        vm.travellerCities = DataMaster.getTravellerCities(res.data.selectedCities.split(','));
        vm.cities = DataMaster.getNonSelectedCities(resolvedData.allCities, [...res.data.selectedCities.split(',')]);
      }
    });
  }

  highlightCityWithId(cityId) {
    const vm = this;
    const highlightCity = (city) => (city.highlighted = city.cityId.toString() === cityId.toString());
    vm.travellerCities.forEach(highlightCity);
    vm.wizardCities.forEach(highlightCity);
  }

  addWizardCity() {
    let vm = this;
    const wizardCityToAdd = _.result(vm, 'wizardCitytoAdd.originalObject');
    if (!wizardCityToAdd || vm.DataMaster.checkIfCityExists(vm.cities, wizardCityToAdd)) {
      sweetAlert("Oops...", "City not found or already added", "error");
      return;
    }
    const wizardCities = [...vm.wizardCities];
    wizardCities.push(wizardCityToAdd);
    vm.updateWizardCities(wizardCities, 'ADD', () => {
      vm.wizardCities.push(wizardCityToAdd);
      const wizardCitytoAddIndex = vm.cities.indexOf(wizardCityToAdd);
      vm.cities.splice(wizardCitytoAddIndex, 1); // remove wizardCitytoAddfrom list of cities
      vm.wizardCitytoAdd = {};
    });
  }
  updateWizardCities(wizardCities, updateType, cb) {
    let vm = this;
    const cityIds = vm.DataMaster.convertCityToIds(wizardCities);
    let cityKeyToChange = (updateType === 'REMOVE' && vm.citytoRemove.cityChosenBy === 'TRAVELLER') ? 'selectedCities' : 'wizardCities';
    vm.$sails.post(`/traveller/${vm.$stateParams.travellerID}`, {
      [cityKeyToChange]: cityIds,
      highlightedCity: vm.DataMaster.getHighlightedCity([...vm.travellerCities, ...vm.wizardCities])
    }).success(cb)
      .error(err => console.error('---in api---', err));
  }
  confirmRemove(cityToRemove, cityChosenBy) {
    let vm = this;
    vm.modalVisibility = true;
    vm.citytoRemove = {
      cityChosenBy,
      city: cityToRemove
    };
  }

  removeWizardCity() {
    let vm = this;
    const originalCities = vm.citytoRemove.cityChosenBy === 'WIZARD' ? vm.wizardCities : vm.travellerCities;
    const clonedCities = [...originalCities];
    const citytoRemoveIndex = clonedCities.indexOf(vm.citytoRemove.city);
    clonedCities.splice(citytoRemoveIndex, 1);
    vm.modalVisibility = false;
    originalCities.splice(citytoRemoveIndex, 1);
    vm.cities.push(vm.citytoRemove.city); //show the city on dropdown now

    vm.updateWizardCities(clonedCities, 'REMOVE', () => {
    });
  }

  toggleCity(city) {
    let vm = this;
    vm.$sails.post(`/traveller/${this.$stateParams.travellerID}`, {
      highlightedCity: city.highlighted ? '' : city.cityId
    }).success(() => {
      if (city.highlighted) {
        city.highlighted = false;
      } else {
        vm.highlightCityWithId(city.cityId);
      }
    });
  }

  highlightZone(zone) {
    const zoneIndex = this.getHighlightedZoneIndex(zone);
    if (zoneIndex > -1) {
      this.highlightedZones.splice(zoneIndex, 1);
    } else {
      this.highlightedZones.push(zone.zoneName);
    }
    this.$sails.post(`/traveller/${this.$stateParams.travellerID}`, {
      visibleZones: this.highlightedZones.join(',')
    });
  }
  getHighlightedZoneIndex(zone) {
    return this.highlightedZones.indexOf(zone.zoneName);
  }
  toggleTravellerCity() {
    this.modalVisibility = true;
  }
}

export default TravellerController;
