const data = function($http, API_URL) {
  'ngInject';
  let allCities = [];
  $http.get(`${API_URL}/fixtures/cities.json`).then(res => {
    allCities = res.data;
  });
  const getAllZones = () => ($http.get(`${API_URL}/fixtures/zones.json`).then(res => res.data));
  const getAllCities = () => ($http.get(`${API_URL}/fixtures/cities.json`));

  const getcitiesFromIds = (cityIDs, cityChosenBy) => {
    return allCities
      .filter(city => cityIDs.indexOf(city.cityId.toString()) > -1)
      .map(crunchData(cityChosenBy));
  };

  const crunchData = (cityChosenBy) => (city) => {
    const iconColor = cityChosenBy === 'WIZARD' ? 'red' : 'blue';
    return Object.assign({}, city, {
      position: [city.latitude, city.longitude],
      highlighted: false,
      iconURL: `./app/icons/marker_${iconColor}.svg`,
      id: `marker-${city.cityId}`,
      cityChosenBy
    });
  };

  const getTravellerCities = () => {
    const getRandomCities = (cities, number) => (_.shuffle(cities).splice(0, number));
    return getAllCities().then(res => getRandomCities(res.data, 5).map(crunchData('TRAVELLER')));
  };

  const getUpdatedCities = (cities, cityChosenBy, newCities) => {
    const otherCities = cities.filter(city => city.cityChosenBy !== cityChosenBy); //return all cities except the cityChosenBy
    const newCrunchedCities = getcitiesFromIds(newCities.split(','), cityChosenBy); //newCities.split(',') because are sent in the form of string, eg:'3,7,15' instead of[3,7,15]
    return [...otherCities, ...newCrunchedCities];
  };

  return {
    getTravellerCities,
    getAllZones,
    getcitiesFromIds,
    getUpdatedCities
  };
};

export default data;
