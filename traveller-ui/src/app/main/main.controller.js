class MainController {
  constructor(NgMap, resolvedData, $http, $sails, DataMaster) {
    'ngInject';
    let vm = this;
    vm.$sails = $sails;
    vm.NgMap = NgMap;
    vm.DataMaster = DataMaster;
    vm.resolvedData = resolvedData;
    vm.travellerCities = [];
    vm.zones = resolvedData.zones;
    vm.userID = '';
    vm.username = '';
    vm.isloggedIn = false;
    vm.username = '';
    vm.infoWindow = {
      text: '',
    };
    vm.googleMapsUrl = 'https://maps.google.com/maps/api/js?key=AIzaSyCiEcfp3t_iqJV__G6aLxaqvuicoezKSpo';
    vm.visibleZones = [];

    vm.markerClicked = (evnt, city) => {
      $sails.post(`/traveller/${vm.userID}`, {
        highlightedCity: city.highlighted ? '' : city.cityId
      }).success(() => {
        vm.highlightMarker(city);
      }).error(() => sweetAlert("Oops...", "Error highlighting marker! Please retry.", "error"));
    };

    vm.resetMap = () => {
      $sails.post(`/traveller/${vm.userID}`, {
        highlightedCity: ''
      }).success(() => {
        vm.closeInfoWindow();
      }).error(() => sweetAlert("Oops...", "Error resetting marker! Please retry.", "error"));
    };

    $sails.on('traveller', (res) => {
      // console.log('log:', 'res', res);
      switch (res.verb) {
        case 'updated': {
          vm.visibleZones = 'visibleZones' in res.data ? res.data.visibleZones : vm.visibleZones;
          vm.travellerCities = 'wizardCities' in res.data ? vm.DataMaster.getUpdatedCities(vm.travellerCities, 'WIZARD', res.data.wizardCities) : vm.travellerCities;
          vm.travellerCities = 'selectedCities' in res.data ? vm.DataMaster.getUpdatedCities(vm.travellerCities, 'TRAVELLER', res.data.selectedCities) : vm.travellerCities;
          if ('highlightedCity' in res.data) {
            vm.prepopulateMarker(res.data.highlightedCity);
          }
        }
      }
    });
  }

  prepopulateMarker(highlightedCity) {
    const vm = this;
    if (highlightedCity === '') {
      vm.closeInfoWindow();
    } else {
      const cityToHighlight = _.find(vm.travellerCities, (city) => (city.cityId.toString() === highlightedCity.toString()));
      vm.highlightMarker(cityToHighlight);
    }
  }

  closeInfoWindow() {
    const vm = this;
    vm.NgMap.getMap({
      id: 'map_canvas'
    }).then((map) => {
      map.hideInfoWindow('info-window');
    });
    vm.travellerCities.forEach(city => {
      city.highlighted = false;
      city.iconURL = city.iconURL.replace('_highlighted', '');
    });
  }
  signup() {
    const vm = this;
    console.log('log:', 'vm.resolvedData.travellerCities', vm.resolvedData.travellerCities);
    vm.$sails.post('/traveller', {
      name: vm.username,
      selectedCities: vm.resolvedData.travellerCities.map(city => city.cityId).join(','),
      visibleZones: ''
    }).success((res) => {
      vm.isloggedIn = true;
      vm.userID = res.id;
      vm.travellerCities = vm.resolvedData.travellerCities;
    }).error(() => sweetAlert("Oops...", "Error signing up! Please retry.", "error"));
  }

  login() {
    const vm = this;
    vm.$sails.get(`/traveller/${vm.userID}`)
      .success(res => {
        vm.username = res.name;
        vm.userID = res.id;
        vm.isloggedIn = true;
        vm.visibleZones = res.visibleZones;
        vm.travellerCities = [...vm.DataMaster.getcitiesFromIds(res.selectedCities, 'TRAVELLER'), ...vm.DataMaster.getcitiesFromIds(res.wizardCities, 'WIZARD')];
        vm.prepopulateMarker(res.highlightedCity);
      })
      .error(() => sweetAlert("Oops...", "Error getting user data! Please retry.", "error"));
  }

  isZoneVisible(zoneName) {
    return this.visibleZones.indexOf(zoneName) > -1;
  }

  toggleMarker(foundFlag, cityID) {
    this.NgMap.getMap({
      id: 'map_canvas'
    }).then(function(map) {
      if (!foundFlag) {
        map.hideInfoWindow('info-window');
      } else {
        map.showInfoWindow('info-window', cityID);
      }
    });
  }

  highlightMarker(city) {
    const vm = this;
    if (!city) {
      return;
    }
    const index = vm.travellerCities.indexOf(city);
    const cityID = city.id;
    let foundFlag = false;
    vm.travellerCities.forEach((city, i) => {
      if (i === index && !city.highlighted) {
        foundFlag = true;
        this.infoWindow.text = city.cityName;
        city.highlighted = true;
        city.iconURL = city.iconURL.replace('.svg', '_highlighted.svg');
      } else {
        city.highlighted = false;
        city.iconURL = city.iconURL.replace('_highlighted', '');
      }
    });
    vm.toggleMarker(foundFlag, cityID);
  }
}

export default MainController;
