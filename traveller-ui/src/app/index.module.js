import config from './index.config';

import routerConfig from './index.route';

import runBlock from './index.run';
import MainController from './main/main.controller';
import DataService from './dataStore/data.service';
angular.module('client', ['ngTouch', 'ui.router', 'ngMap', 'ngSails'])
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .controller('MainController', MainController)
  .service('DataMaster', DataService)
  .constant('API_URL', location.origin.indexOf('localhost') > -1 ? 'http://localhost:1337' : location.origin);
