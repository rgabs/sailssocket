function config($sailsProvider, API_URL) {
  'ngInject';
  $sailsProvider.url = API_URL;
}

export default config;
