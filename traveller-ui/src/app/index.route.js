function routerConfig($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'app/main/main.html',
      controller: 'MainController',
      controllerAs: 'main',
      resolve: {
        resolvedData(DataMaster) {
          const travellerCities = DataMaster.getTravellerCities();
          const zones = DataMaster.getAllZones();
          return Promise.all([travellerCities, zones]).then(res => ({
            travellerCities: res[0],
            zones: res[1]
          }));
        }
      }
    });

  $urlRouterProvider.otherwise('/');
}

export default routerConfig;
