/**
* Traveller.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    'name': {
      type: 'string',
      required: true
    },
    selectedCities: { //comma separated id's
      type: 'string',
      defaultsTo: ''
    },
    online: {
      type: 'boolean',
      defaultsTo: false
    },
    wizardCities: {
      type: 'string',
      defaultsTo: ''
    },
    visibleZones: {
      type: 'string',
      defaultsTo: ''
    },
    highlightedCity: {
      type: 'string',
      defaultsTo: ''
    },
    toJSON: function() {
      let obj = this.toObject();
      console.log('log:', 'obj.selectedCities', obj.selectedCities);
      obj.selectedCities = obj.selectedCities ? obj.selectedCities.split(',') : [];
      obj.visibleZones = obj.visibleZones ? obj.visibleZones.split(',') : [];
      obj.wizardCities = obj.wizardCities ? obj.wizardCities.split(',') : [];
      return obj;
    }
  }
};
